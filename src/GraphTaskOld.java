/*
 * materjale:
 * 
 * 
 * sügis2013 nõuanded https://echo360.e-ope.ee/ess/echo/presentation/384cf2e4-0cbc-41da-bbdd-4e83626f1483?ec=true
 * 
 * 						- http://enos.itcollege.ee/~jpoial/algoritmid/graafid.html
 * Dijkstra algoritm	- https://en.wikipedia.org/wiki/Dijkstra%27s_algorithm
 * 						- example https://www.youtube.com/watch?v=0nVYi3o161A
 * 						- example https://www.youtube.com/watch?v=dS1Di2ZHl4k
 * Linkedlist			- yt https://www.youtube.com/watch?v=9UJ2XtgRtMk
 * Graph glossray		- https://en.wikipedia.org/wiki/Glossary_of_graph_theory
 * Shortest Path		- https://en.wikipedia.org/wiki/Shortest_path_problem
 * Path					- https://en.wikipedia.org/wiki/Path_(graph_theory)
 * 						- https://www.youtube.com/results?search_query=single-pair+shortest+path+problem&page=&utm_source=opensearch
 * MIT 15. Single-Source Shortest Paths Problem	- https://www.youtube.com/watch?v=Aa2sqUhIn-E
 * 								dijkstra		- https://www.youtube.com/watch?v=2E7MmKv0Y24
 * 				dijkstra - http://www.cs.cornell.edu/~wdtseng/icpc/notes/graph_part2.pdf
 * Pöial kauguste maatrix @30min - https://echo360.e-ope.ee/ess/echo/presentation/83fb1c75-8f0b-4dd1-8eb5-33b5911a2a41?ec=true
 * Pöial kodutöö seletus - https://echo360.e-ope.ee/ess/echo/presentation/bb75f754-1bb5-4854-b697-22c6dfc13872?ec=true
 * 
 * 						- http://www.vogella.com/tutorials/JavaAlgorithmsDijkstra/article.html
 **** 15. Koostada meetod, mis leiab etteantud sidusas lihtgraafis  ****
 **** kahe etteantud tipu vahelise LÜHIMA TEE. *************************
 * 
 * The problem is also sometimes called the single-pair shortest path problem, 
 * to distinguish it from other variations.
 * 
 * Kõigi ülesannete puhul tuleb kasutada Moodle'st võetud programmitoorikut 
 * (klassid Graph, Vertex, Edge). Vajadusel tohib nendes klassides lisada isendimuutujaid ja meetodeid. 
 * Aruandes peab olema vähemalt 5 testi.
 * 
 * Simple graph: undirected, without parallel edges and self-loops.
 * strongly connected
 * Floyd-Warshall		- https://en.wikipedia.org/wiki/Floyd%E2%80%93Warshall_algorithm
 * Dijkstra				- https://en.wikipedia.org/wiki/Dijkstra%27s_algorithm
 * Graph traversals: depth-first and breadth-first search.
 * 
 * G = (V, E, W) - graaf
 *      v - verteces
 *         e - edges
 *            w - weights
 * V - lõplik tippude hulk
 * 
 * Silmuste ja kordsete servadeta orienteerimata graafi nim. lihtgraafiks.
 * 
 * Orienteerimata graafi nim. sidusaks, kui leidub tee mistahes tipust mistahes teise tippu.
 * 
 */
import java.util.*;

public class GraphTaskOld {

   public static void main (String[] args) {
      GraphTaskOld a = new GraphTaskOld(); // creates a new GraphTask
      a.run(); // runs the created GraphTask
//      throw new RuntimeException ("Nothing implemented yet!"); // delete this
   }

   public void run() {
      Graph g = new Graph ("G"); // creates new Graph named ""
      g.createRandomSimpleGraph (4, 5); // inserts a RandomSimpleGraph with (6) Vertices, (9) Edges
      System.out.println (g); // kogu väljaprinditav graaf

      // TODO!!! YOUR TESTS HERE!
   }


	class Vertex { // Vertex

		String id;
		String name; // ise
		Vertex next;
		Edge first;
		int info = 0;
		int weight = Integer.MAX_VALUE; // weight

		Vertex(String s, Vertex v, Edge e) { // Vertex constructor: id, next Vertex, first Edge (väärtustab)
			id = s;
			next = v;
			first = e;
		}

		Vertex(String id, String name) { // Vertex constructor: id, next Vertex, first Edge (väärtustab)
			this.id = id;
			this.name = name;
		}

		Vertex(String s) {
			this(s, null, null);
		}

//      @Override
//      public String toString() {
//         return id;
//      }
      
      public String getId() {
    	    return id;
    	  }

    	  public String getName() {
    	    return name;
    	  }
    	  
    	  @Override
    	  public int hashCode() {
    	    final int prime = 31;
    	    int result = 1;
    	    result = prime * result + ((id == null) ? 0 : id.hashCode());
    	    return result;
    	  }
    	  
    	  @Override
    	  public boolean equals(Object obj) {
    	    if (this == obj)
    	      return true;
    	    if (obj == null)
    	      return false;
    	    if (getClass() != obj.getClass())
    	      return false;
    	    Vertex other = (Vertex) obj;
    	    if (id == null) {
    	      if (other.id != null)
    	        return false;
    	    } else if (!id.equals(other.id))
    	      return false;
    	    return true;
    	  }

    	  @Override
    	  public String toString() {
    	    return name;
    	  }
    	  

      // TODO!!! Your Vertex methods here!

   } // Vertex


   class Edge { // Edge

      String id;
      Vertex source; // ise source
      Vertex target; // destination where this Edge drains to (Vertex)
      Edge next; // next Edge from the same Vertex
      int info = 0;
      int weight = 0;

      Edge (String s, Vertex v, Edge e) { // Edge constructor: id, target Vertex, next Edge, length (väärtustab)
         id = s;
//         source = src; // ise
         target = v;
         next = e;
//         weight = w;
		}

		Edge(String s) { // Edge constructor: id, (target Vertex), (next Edge), (length)
			this(s, null, null);
		}

		Edge(String id, Vertex source, Vertex target, int weight) {
			this.id = id;
			this.source = source;
			this.target = target;
			this.weight = weight;
		}

//      @Override
//      public String toString() {
//         return id;
//      }
      
      public String getId() {
    	    return id;
    	  }
    	  public Vertex getDestination() {
    	    return target;
    	  }

    	  public Vertex getSource() {
    	    return source;
    	  }
    	  public int getWeight() {
    	    return weight;
    	  }
    	  
    	  @Override
    	  public String toString() {
    	    return source + " " + target;
    	  }

      // TODO!!! Your Edge methods here!

   } // Edge


   class Graph { // Graph

	   /////////// articleist
	   
	   List<Vertex> vertexes; // private final
	   List<Edge> edges; // private final

	   public Graph(List<Vertex> vertexes, List<Edge> edges) {
	     this.vertexes = vertexes;
	     this.edges = edges;
	   }

	   public List<Vertex> getVertexes() {
	     return vertexes;
	   }

	   public List<Edge> getEdges() {
	     return edges;
	   }
	   
	   
	   
	   
	   /////////////
      String id;
      Vertex first;
      int info = 0;

      Graph (String s, Vertex v) { // Graph constructor: id, first Vertex
         id = s;
         first = v;
      }

      Graph (String s) { // Graph constructor: id, (first Vertex)
         this (s, null);
      }

      @Override
      public String toString() { // Graph toString
         String ls = System.getProperty ("line.separator");
         StringBuilder sb = new StringBuilder (ls);
         sb.append (id); // prints Graph name
         sb.append (ls); // line separator after Graph name
         Vertex v = first; // Vertex first
         while (v != null) {
            sb.append (v.toString()); // prints Vertex name
            sb.append (" -->");
            Edge e = v.first; // Edge first
            while (e != null) {
               sb.append (" ");
               sb.append (e.toString());
               sb.append (" (");
               sb.append (v.toString());
               sb.append ("->");
               sb.append (e.target.toString());
               sb.append (")");
               e = e.next;
            }
            sb.append (ls);
            v = v.next;
         }
         return sb.toString();
      }

      public Vertex createVertex (String vid) {
         Vertex res = new Vertex (vid);
         res.next = first;
         first = res;
         return res;
      }

      public Edge createArc (String eid, Vertex from, Vertex to) {
         Edge res = new Edge (eid);
         res.next = from.first;
         from.first = res;
         res.target = to;
         return res;
      }

      /**
       * Create a connected undirected random tree with n vertices.
       * Each new vertex is connected to some random existing vertex.
       * @param n number of vertices added to this graph
       */
      public void createRandomTree (int n) {
         if (n <= 0)
            return;
         Vertex[] varray = new Vertex [n];
         for (int i = 0; i < n; i++) {
            varray [i] = createVertex ("v" + String.valueOf(n-i));
            if (i > 0) {
               int vnr = (int)(Math.random()*i);
               createArc ("e" + varray [vnr].toString() + "_"
                  + varray [i].toString(), varray [vnr], varray [i]);
               createArc ("e" + varray [i].toString() + "_"
                  + varray [vnr].toString(), varray [i], varray [vnr]);
            } else {}
         }
      }

      /**
       * Create an adjacency matrix of this graph.
       * Side effect: corrupts info fields in the graph
       * @return adjacency matrix
       */
      public int[][] createAdjMatrix() {
         info = 0;
         Vertex v = first;
         while (v != null) {
            v.info = info++;
            v = v.next;
         }
         int[][] res = new int [info][info];
         v = first;
         while (v != null) {
            int i = v.info;
            Edge e = v.first;
            while (e != null) {
               int j = e.target.info;
               res [i][j]++;
               e = e.next;
            }
            v = v.next;
         }
         return res;
      }

      /**
       * Create a connected simple (undirected, no loops, no multiple
       * edges) random graph with n vertices and m edges.
       * @param n number of vertices
       * @param m number of edges
       */
      public void createRandomSimpleGraph (int n, int m) {
         if (n <= 0)
            return;
         if (n > 2500)
            throw new IllegalArgumentException ("Too many vertices: " + n);
         if (m < n-1 || m > n*(n-1)/2)
            throw new IllegalArgumentException 
               ("Impossible number of edges: " + m);
         first = null;
         createRandomTree (n);       // n-1 edges created here
         Vertex[] vert = new Vertex [n];
         Vertex v = first;
         int c = 0;
         while (v != null) {
            vert[c++] = v;
            v = v.next;
         }
         int[][] connected = createAdjMatrix();
         int edgeCount = m - n + 1;  // remaining edges
         while (edgeCount > 0) {
            int i = (int)(Math.random()*n);  // random source
            int j = (int)(Math.random()*n);  // random target
            if (i==j) 
               continue;  // no loops
            if (connected [i][j] != 0 || connected [j][i] != 0) 
               continue;  // no multiple edges
            Vertex vi = vert [i];
            Vertex vj = vert [j];
            createArc ("e" + vi.toString() + "_" + vj.toString(), vi, vj);
            connected [i][j] = 1;
            createArc ("e" + vj.toString() + "_" + vi.toString(), vj, vi);
            connected [j][i] = 1;
            edgeCount--;  // a new edge happily created
         }
      }
      
      
      
      private List<Vertex> nodes;
//      private final List<Edge> edges;
      private Set<Vertex> settledNodes;
      private Set<Vertex> unSettledNodes;
      private Map<Vertex, Vertex> predecessors;
      private Map<Vertex, Integer> distance;

      public void DijkstraAlgorithm(Graph graph) {
        // create a copy of the array so that we can operate on this array
        this.nodes = new ArrayList<Vertex>(graph.getVertexes());
        this.edges = new ArrayList<Edge>(graph.getEdges());
      }

      public void execute(Vertex source) {
        settledNodes = new HashSet<Vertex>();
        unSettledNodes = new HashSet<Vertex>();
        distance = new HashMap<Vertex, Integer>();
        predecessors = new HashMap<Vertex, Vertex>();
        distance.put(source, 0);
        unSettledNodes.add(source);
        while (unSettledNodes.size() > 0) {
          Vertex node = getMinimum(unSettledNodes);
          settledNodes.add(node);
          unSettledNodes.remove(node);
          findMinimalDistances(node);
        }
      }

      private void findMinimalDistances(Vertex node) {
        List<Vertex> adjacentNodes = getNeighbors(node);
        for (Vertex target : adjacentNodes) {
          if (getShortestDistance(target) > getShortestDistance(node)
              + getDistance(node, target)) {
            distance.put(target, getShortestDistance(node)
                + getDistance(node, target));
            predecessors.put(target, node);
            unSettledNodes.add(target);
          }
        }

      }

      private int getDistance(Vertex node, Vertex target) {
        for (Edge edge : edges) {
          if (edge.getSource().equals(node)
              && edge.getDestination().equals(target)) {
            return edge.getWeight();
          }
        }
        throw new RuntimeException("Should not happen");
      }

      private List<Vertex> getNeighbors(Vertex node) {
        List<Vertex> neighbors = new ArrayList<Vertex>();
        for (Edge edge : edges) {
          if (edge.getSource().equals(node)
              && !isSettled(edge.getDestination())) {
            neighbors.add(edge.getDestination());
          }
        }
        return neighbors;
      }

      private Vertex getMinimum(Set<Vertex> vertexes) {
        Vertex minimum = null;
        for (Vertex vertex : vertexes) {
          if (minimum == null) {
            minimum = vertex;
          } else {
            if (getShortestDistance(vertex) < getShortestDistance(minimum)) {
              minimum = vertex;
            }
          }
        }
        return minimum;
      }

      private boolean isSettled(Vertex vertex) {
        return settledNodes.contains(vertex);
      }

      private int getShortestDistance(Vertex destination) {
        Integer d = distance.get(destination);
        if (d == null) {
          return Integer.MAX_VALUE;
        } else {
          return d;
        }
      }

      /*
       * This method returns the path from the source to the selected target and
       * NULL if no path exists
       */
      public LinkedList<Vertex> getPath(Vertex target) {
        LinkedList<Vertex> path = new LinkedList<Vertex>();
        Vertex step = target;
        // check if a path exists
        if (predecessors.get(step) == null) {
          return null;
        }
        path.add(step);
        while (predecessors.get(step) != null) {
          step = predecessors.get(step);
          path.add(step);
        }
        // Put it into the correct order
        Collections.reverse(path);
        return path;
      }
      
      
      
//		public void simple() {
//			Vertex v0 = new Vertex("red");
//			Vertex v1 = new Vertex("green");
//			Vertex v2 = new Vertex("blue");
//			Vertex v3 = new Vertex("black");
//			Vertex v4 = new Vertex("white");
//
//    		v0.adjacencies = new Edge[]{ new Edge(v1, 5),
//    		                             new Edge(v2, 10),
//    	                               new Edge(v3, 8) };
//    		v1.adjacencies = new Edge[]{ new Edge(v0, 5),
//    		                             new Edge(v2, 3),
//    		                             new Edge(v4, 7) };
//    		v2.adjacencies = new Edge[]{ new Edge(v0, 10),
//    	                               new Edge(v1, 3) };
//    		v3.adjacencies = new Edge[]{ new Edge(v0, 8),
//    		                             new Edge(v4, 2) };
//    		v4.adjacencies = new Edge[]{ new Edge(v1, 7),
//    	                               new Edge(v3, 2) };
//    		Vertex[] vertices = { v0, v1, v2, v3, v4 };
//       }

      // TODO!!! Your Graph methods here!

      
      
      // list of path to take
//   public static List<Vertex> getShortestPathTo(Vertex target)
//   {
//       List<Vertex> path = new ArrayList<Vertex>();
//       for (Vertex vertex = target; vertex != null; vertex = vertex.previous)
//           path.add(vertex);
//       Collections.reverse(path);
//       return path;
//   }

   
   } // Graph
   
   

} // GraphTask

