package ee.itcollege.llaidna;

import org.junit.Test;
import java.util.*;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class TestDijkstraAlgorithm {

	private List<Verteks> nodes;
	private List<Edke> edges;

	@Test
	public void testExcute() {
		nodes = new ArrayList<Verteks>();
		edges = new ArrayList<Edke>();
		int numberofnodes = 10;  // loo x tippu (Vertex, Node)
		int from = 1; // algtipp 217+10+167 = 394
		int to = 9; // lõpptipp
		for (int i = 0; i < numberofnodes; i++) {
			Verteks location = new Verteks("tipp" + i, "tipp" + i);
			nodes.add(location); // lisa loodud (location) Vertex nodes ArrayListi
//			System.out.println("added to nodes: " + location + " & nodes is: " + nodes);
		}
		
		// String laneId, int sourceLocNo, int destLocNo, int duration
		addLane("Edge00", 0, 1, 85);
		addLane("Edge01", 1, 2, 217);
		addLane("Edge02", 2, 7, 10);
		addLane("Edge03", 2, 3, 100);
		addLane("Edge04", 2, 7, 5);
		addLane("Edge05", 3, 7, 183);
		addLane("Edge06", 5, 8, 250);
		addLane("Edge07", 8, 9, 84);
		addLane("Edge08", 7, 9, 167);
		addLane("Edge09", 4, 9, 502);
		addLane("Edge10", 1, 9, 4000);
		addLane("Edge11", 1, 9, 600);

		Kraph graph = new Kraph(nodes, edges); // loo uus Graph, mis koosneb ArrayList<Vertex> nodes, ArrayList<Edge> edges
		GraphTask dijkstra = new GraphTask(graph); // teeb graph'ist koopia töötlemiseks
		dijkstra.execute(nodes.get(from)); // määra alguse vertex
		LinkedList<Verteks> path = dijkstra.getPath(nodes.get(to)); // määra lõpp vertex

		assertNotNull(path);
		assertTrue(path.size() > 0);
		
		System.out.println("Graafi tipud on: " + graph.getVertexes());
		System.out.println("Graafi servad on: " + graph.getEdges());
		System.out.println();
		System.out.println("------------------------- Lühim tee ----------------------------");
		System.out.println("Läbitavaid tippe kokku: " + path.size());
		System.out.println("");
		System.out.println("1. tipp: " + path.get(0));
		
		int distance;
		int total = 0;
		
		for (int i = 1; i < path.size(); i++) {
			distance = dijkstra.getDistance(path.get(i - 1), path.get(i));
			total += distance;
			System.out.println(i + 1 + ". tipp: " + path.get(i) + " kaugus eelmisest: " + distance);
		}

		System.out.println("");
		System.out.println("Algtipu " + path.getFirst() + " ja lõpptipu " + path.getLast() + " vaheline kaugus kokku: " + total);
		System.out.println("----------------------------------------------------------------");

	}

	/**
	 * Meetod lisab node'ide vahele servad. Lisab servad mõlemat pidi, et oleks orienteerimata graaf 
	 * 
	 * @param laneId String tüüpi - Edge'i nimi
	 * @param sourceLocNo int tüüpi - Node'i number
	 * @param destLocNo int tüüpi - Node'i number
	 * @param duration int tüüpi - kaugus
	 * @author vogella
	 * @author lauri
	 */
	private void addLane(String laneId, int sourceLocNo, int destLocNo, int duration) {
		Edke lane = new Edke(laneId, nodes.get(sourceLocNo), nodes.get(destLocNo), duration);
		edges.add(lane); // lisa serv ArrayListi
		Edke lane2 = new Edke(laneId + "2", nodes.get(destLocNo), nodes.get(sourceLocNo), duration); // pöörab serva, et graaf oleks orienteerimata
		edges.add(lane2); // lisa pööratud serv ArrayListi
		// System.out.println("edges ArrayList is: " + edges);
	}
}