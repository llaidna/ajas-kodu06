package ee.itcollege.llaidna;

/* ********************************************************************* *
 * *** 15. Koostada meetod, mis leiab etteantud sidusas lihtgraafis  *** *
 * ***     kahe etteantud tipu vahelise LyHIMA TEE.                  *** *
 * ********************************************************************* * 
 * 
 * Lauri bitbucket https://bitbucket.org/llaidna/ajas-kodu06/
 * 
 * T2helepanu! Loe, enne kui j2tkad!
 * 
 * K2esolev lahenduse kood on suures enamuses kopeeritud ja viidatud ning pole tudengi oma looming. 
 * T88 sellisel kujul esitamise peamine m6te on v6imaldada dokumentatsiooni koostamist, mis ilma 
 * toimiva koodita oleks v6imatu. Eksisteerib m6ningane lootus, et kaitsmise ajaks on ka kood muutunud 
 * rohkem tudengi enda kirjutatuks, v6imete ja oskuste piires (piiril).
 * 
 * Koodi originaali autor vogella, http://www.vogella.com/tutorials/JavaAlgorithmsDijkstra/article.html
 * 
 */

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import ee.itcollege.llaidna.GraphTask.Graph;

/**
 * GraphTask class - sisaldab endas peamisi meetodeid ja main meetodit.
 * 
 * @author vogella - peamine Dijkstra kood
 * @author lauri - mugandused, k2ideldavus, argumentide sisestamine, tulemuse printimine
 */
public class GraphTask {

	@SuppressWarnings("unused")
	private final List<Verteks> nodes;
	private final List<Edke> edges;
	private Set<Verteks> settledNodes;
	private Set<Verteks> unSettledNodes;
	private Map<Verteks, Verteks> predecessors;
	Map<Verteks, Integer> distance; // oli private

	private static List<Verteks> nodesIn;
	private static List<Edke> edgesIn;
	

	private static List<Integer> servad = new ArrayList<Integer>();; // j.pöiali koodi toimimasaamiseks. List serva tippudest, mis ühenduvad
	
	public static int numberofnodes;
	public static int numberofedges;


	/**
	 * Teeb t88tlmiseks koopia tippudest ja servadest.
	 * 
	 * @param kraph Graph tyypi
	 * @author vogella
	 */
	public GraphTask(Kraph kraph) {
		this.nodes = new ArrayList<Verteks>(kraph.getVertexes()); // koopia sissetulnud graph'i nodes'itest
		this.edges = new ArrayList<Edke>(kraph.getEdges()); // koopia sissetulnud graph'i edges'itest
	}
	
	
	
	
	/**
	 * Main meetod programmi k2ivitamiseks. V6tab vastu sisendparameetreid k2surealt. Vaata kasutusjuhendit!
	 * 
	 * @param args String tyypi sisendiparameetrid programmi andmete sisse kandmiseks
	 * @author lauri
	 * @author vogella - m6ned read koodi
	 */
	public static void main(String[] args) {

		//////////// j.pöidla kohustuslik koodijupp //////////////////////
//	      GraphTask a = new GraphTask();
//	      a.run();
		//////////////////////////////////////////////////////////////////////
		
		nodesIn = new ArrayList<Verteks>();
		edgesIn = new ArrayList<Edke>();

		System.out.println("Sisendi kontroll... ");
		
		// kui pole sisestatud ühtegi argumenti, kuvame 6petuse
		if (args.length == 0) {
			System.out.println("Palun sisesta argumentidena, eraldades tyhikuga j2rgnev info:");
			System.out.println();
			System.out.println("t s t1 t2 k /.../ t1 t2 k a l");
			System.out.println();
			System.out.println("t  - tippude hulk");
			System.out.println("s  - servade hulk");
			System.out.println("t1 - serva ühe tipu number");
			System.out.println("t2 - serva teise tipu number");
			System.out.println("k  - serva pikkus ehk tippude vaheline kaugus");
			System.out.println("a  - otsitava lyhima tee tee alguse tipu number");
			System.out.println("l  - otsitava lyhima tee tee l6pu tipu number");
			System.out.println();
			System.out.println("NB! Tippe loendame 0, 1, 2, 3... !");
			return;
		}
		
		for (int i = 0; i < args.length; i++) {
			try {
				Integer.parseInt(args[i]);
			} catch (NumberFormatException e) {
				e.printStackTrace();
				System.out.println("Sisestasid vigased argumendid. Argumendid tohivad olla ainult numbrid, nullist suuremad, eraldatud tyhikuga!");
			}
		}

		// sisestatud v22rtused peavad olema nullist suuremad!
		for (int i = 0; i < args.length; i++) {
			if (Integer.parseInt(args[i]) < 0) {
				System.out.println("K6ik sisestatud v22rtused peavad olema nullist suuremad! Sisestasid: " + args[i]);
				return;
			}
		}

		// sisestama peab v2hemalt sama palju servi, kui on tippe miinus yks
		if (Integer.parseInt(args[0]) - Integer.parseInt(args[1]) >= 2) {
			System.out.println("Sisestasid liiga v2he servi. Sidusa graafi loomiseks on vaja v2hemalt sama palju servi, kui on tippe miinus yks.");
			System.out.println("Sisestasid " + Integer.parseInt(args[0]) + " tippu, aga ainult " + Integer.parseInt(args[1]) + " serva");
			return;
		}

		// sisestasid vale arvu argumente
		if ((4 + Integer.parseInt(args[1]) * 3) != args.length) {
			System.out.println("Sisestasid liiga v2he v6i liiga palju argumente! Argumente peab olema (4 + 3 * soovitudHulkServi), sina aga sisestasid " + args.length + " argumenti.");
			return;
		}

//		// algtipp ja l6pptipp ei tohi olla samad!
//		for (int i = 0; i < args.length - 3; i++) {
//			if (Integer.parseInt(args[i + 2]) == Integer.parseInt(args[i + 3])) {
//				System.out.println("Algtipp ja l6pptipp ei tohi olla samad! Tekib silmus, mis pole selle ylesande raames lubatud! Sisestasid serva, mis yhendab tipud: " + args[i + 2] + " ja "
//						+ args[i + 3]);
//				return;
//			}
//		}

		// yritad siduda serva tipuga, mida pole olemas.
		for (int i = 0; i < Integer.parseInt(args[1]); i++) {
			if (Integer.parseInt(args[2 + i * 3]) >= Integer.parseInt(args[0]) || Integer.parseInt(args[3 + i * 3]) >= Integer.parseInt(args[0])) {
				System.out.println("yritad siduda serva tipuga, mida pole olemas! Pole sisestatud tippu " + args[2 + i * 3] + " v6i " + args[3 + i * 3]);
				return;
			}
		}

		// soovid leida tippu, mida ei ole sisestatud
		if (Integer.parseInt(args[args.length - 1]) >= Integer.parseInt(args[0])) {
			System.out.println("Otsitava tee l6pptippu pole sisestatud! Sisestatud on " + args[0] + " tippu, aga soovid leida tippu " + (Integer.parseInt(args[args.length - 1]) + 1)
					+ " (loendama hakatakse nullist!)");
			return;
		}

		System.out.println("Sisend ok! J2tkame...");
		System.out.println();
		numberofnodes = Integer.parseInt(args[0]);
		numberofedges = Integer.parseInt(args[1]);
		int node1 = 0;
		int node2 = 0;
		int len = 0;
		int from = Integer.parseInt(args[args.length - 2]);
		int to = Integer.parseInt(args[args.length - 1]);

//		Vertex[] varray = new Vertex[numberofnodes]; //j.pöiali

		for (int i = 0; i < numberofnodes; i++) { // loob node'id
			Verteks location = new Verteks("tipp" + i, "tipp" + i);
			nodesIn.add(location); // lisa loodud (location) Vertex nodes ArrayListi
//			varray[i] = createVertex("v" + String.valueOf(numberofnodes - i));  //j.pöiali
		}

		System.out.println("Loodud tipud:              " + nodesIn);

		for (int i = 0; i < numberofedges; i++) { // loob edge'id
			int j = i + 2 + 2 * i;
			node1 = Integer.parseInt(args[j]);
			node2 = Integer.parseInt(args[j + 1]);
			len = Integer.parseInt(args[j + 2]);
			addLaneIn("Edge" + i, node1, node2, len);
			servad.add(node1); // lisab eraldi loendisse serva numbri
			servad.add(node2); // lisab eraldi loendisse serva numbri
//			createArc("e" + node1 + "_" + node2, nodesIn.get(node1), nodesIn.get(node2));
//			createArc("e" + node2 + "_" + node1, node2, node1);
		}

		System.out.println("Sisestatud tippude arv:    " + numberofnodes);
		System.out.println("Sisestatud servade arv:    " + numberofedges);
		System.out.println("Otsitava lyhima tee algus: tipp" + from);
		System.out.println("Otsitava lyhima tee l6pp:  tipp" + to);

		Kraph kraph = new Kraph(nodesIn, edgesIn); // loo uus Graph, mis koosneb ArrayList<Vertex> nodes, ArrayList<Edge> edges
		GraphTask dijkstra = new GraphTask(kraph); // teeb graph'ist koopia t88tlemiseks
		dijkstra.execute(nodesIn.get(from)); // m22ra alguse vertex
		LinkedList<Verteks> path = dijkstra.getPath(nodesIn.get(to)); // m22ra l6pp vertex
		

		try {
			path.size();
		} catch (NullPointerException e) {
			System.out.println("Soovitud teed pole v6imalik leida! Palun kontrolli sisestatud servi. Sisestatud graaf pole sidus!");
			return;
//			e.printStackTrace();
		}
		
		System.out.println("Graafi tipud on:           " + kraph.getVertexes());
		System.out.println("Graafi servad on:          " + kraph.getEdges());
		System.out.println();
		System.out.println("------------------------- Lyhim tee ----------------------------");
		System.out.println("L2bitavaid tippe kokku: " + path.size());
		System.out.println("");
		System.out.println("1. tipp: " + path.get(0));

		int distance;
		int total = 0;

		for (int i = 1; i < path.size(); i++) {
			distance = dijkstra.getDistance(path.get(i - 1), path.get(i));
			total += distance;
			System.out.println(i + 1 + ". tipp: " + path.get(i) + " kaugus eelmisest: " + distance);
		}

		System.out.println("");
		System.out.println("Algtipu " + path.getFirst() + " ja l6pptipu " + path.getLast() + " vaheline kaugus kokku: " + total);
		System.out.println("----------------------------------------------------------------");
		//////////// j.pöidla kohustuslik koodijupp //////////////////////
//	      GraphTask a = new GraphTask();
		dijkstra.run();
		//////////////////////////////////////////////////////////////////////

	}
	
	public void run() {
		Graph g = new Graph("G");
		g.createSimpleGraph(GraphTask.numberofedges, GraphTask.numberofedges);
		System.out.print("createRandomSimpleGraph:");
		System.out.println(g);

		// TODO!!! YOUR TESTS HERE!
	}


	/**
	 * K2ivitab meetodi vastavalt sisestatud tipule
	 * 
	 * @param source Vertex tyypi, m22rab alguse tipu
	 * @author vogella
	 */
	public void execute(Verteks source) {
		settledNodes = new HashSet<Verteks>();
		unSettledNodes = new HashSet<Verteks>();
		distance = new HashMap<Verteks, Integer>();
		predecessors = new HashMap<Verteks, Verteks>();
		distance.put(source, 0);
		unSettledNodes.add(source);
		while (unSettledNodes.size() > 0) { // t88tle kuni enam pole yhtegi unSettledNodes
			Verteks node = getMinimum(unSettledNodes);
			settledNodes.add(node);
			unSettledNodes.remove(node);
			findMinimalDistances(node);
		}
	}

	/**
	 * Leia minimaalsed distantsid sisestatud node'i ja target node'i vahel
	 * 
	 * @param node Vertex
	 * @author vogella
	 */
	private void findMinimalDistances(Verteks node) {
		List<Verteks> adjacentNodes = getNeighbors(node);
		for (Verteks target : adjacentNodes) {
			if (getShortestDistance(target) > getShortestDistance(node) + getDistance(node, target)) {
				distance.put(target, getShortestDistance(node) + getDistance(node, target));
				predecessors.put(target, node);
				unSettledNodes.add(target);
			}
		}

	}

	/**
	 * Leiab kahe tipu vahelise kauguse
	 * 
	 * @param node Vertex tyypi
	 * @param target Vertex tyypi
	 * @return edge.getWeight() int tyypi
	 * @author vogella
	 */
	public int getDistance(Verteks node, Verteks target) { // oli private
		for (Edke edge : edges) {
			if (edge.getSource().equals(node) && edge.getDestination().equals(target)) {
				// System.out.println("edge.getWeight()" + edge.getWeight());
				return edge.getWeight();
			}
		}
		throw new RuntimeException("Should not happen");
	}
  
	/**
	 * Tagastab tipu naabrid
	 * 
	 * @param node Vertex tyypi
	 * @return neighbors Vertex tyypi ArrayList
	 * @author vogella
	 */
	private List<Verteks> getNeighbors(Verteks node) {
//		System.out.println("getNeighbors meetodi sisse: " + node);
		List<Verteks> neighbors = new ArrayList<Verteks>();
		for (Edke edge : edges) {
			if (edge.getSource().equals(node) && !isSettled(edge.getDestination())) {
				neighbors.add(edge.getDestination());
			}
		}
		return neighbors;
	}

	/**
	 * Tagastab etteantud vertexitest miinimumi
	 * 
	 * @param vertexes Vertex tyypi
	 * @return minimum Vertex tyypi
	 * @author vogella
	 */
	private Verteks getMinimum(Set<Verteks> vertexes) {
		Verteks minimum = null;
		for (Verteks vertex : vertexes) {
			if (minimum == null) {
				minimum = vertex;
			} else {
				if (getShortestDistance(vertex) < getShortestDistance(minimum)) {
					minimum = vertex;
				}
			}
		}
		return minimum;
	}

	/**
	 * Kontrollib, kas vertex kuulub settledNodes hulka.
	 * 
	 * @param vertex Vertex tyypi
	 * @return t6ev22rtus true or false
	 */
	private boolean isSettled(Verteks vertex) {
		return settledNodes.contains(vertex);
	}

	/**
	 * Tagastab lyhima distantsi.
	 * 
	 * @param destination Vertex tyypi
	 * @return d Integer tyypi
	 * @author vogella
	 */
	private int getShortestDistance(Verteks destination) {
		Integer d = distance.get(destination);
		if (d == null) {
			return Integer.MAX_VALUE;
		} else {
			return d;
		}
	}

	/**
	 * Meetod tagastab teekonna algpunktist (Vertex) kuni sisestatud punktini target (Vertex)
	 * 
	 * @param target Vertex tyypi
	 * @return path Vertex tyypi LinkedList
	 * @author vogella
	 */
	public LinkedList<Verteks> getPath(Verteks target) {
		LinkedList<Verteks> path = new LinkedList<Verteks>();
		Verteks step = target; // abimuutuja Vertex tyypi
		if (predecessors.get(step) == null) { // kontrollib kas teekond eksisteerib
			return null;
		}
		path.add(step);
//		System.out.println("step: " + step);
		while (predecessors.get(step) != null) {
			step = predecessors.get(step);
//			System.out.println("step: " + step);
			path.add(step);
		}
		Collections.reverse(path); // p88rab LinkedListi ringi
		return path;
	}

	/**
	 * Kaarte loomise meetod
	 * 
	 * @param laneId String tyypi - kaare nimi
	 * @param sourceLocNo int tyypi - kaare algus tipu number
	 * @param destLocNo int tyypi - kaare l6pp tipu number
	 * @param duration int tyypi - kaare pikkus
	 * @author vogella
	 * @author lauri
	 */
	private static void addLaneIn(String laneId, int sourceLocNo, int destLocNo, int duration) {
		Edke lane = new Edke(laneId, nodesIn.get(sourceLocNo), nodesIn.get(destLocNo), duration);
		edgesIn.add(lane); // lisa serv ArrayListi
		// p88rab serva, et graaf oleks orienteerimata
		Edke lane2 = new Edke(laneId + "2", nodesIn.get(destLocNo), nodesIn.get(sourceLocNo), duration);
		edgesIn.add(lane2); // lisa p88ratud serv ArrayListi
	}

	// ////////////////////////////////////////////////////////////////////////siit järgneb j.pöidla koodipõhi /////////////////



	public class Vertex {

		String id;
		Vertex next;
		Edge first;
		int info = 0;

		Vertex(String s, Vertex v, Edge e) {
			id = s;
			next = v;
			first = e;
		}

		Vertex(String s) {
			this(s, null, null);
		}

		@Override
		public String toString() {
			return id;
		}

		// TODO!!! Your Vertex methods here!

	} // Vertex

	public class Edge {

		String id;
		Vertex target;
		Edge next;
		int info = 0;

		Edge(String s, Vertex v, Edge e) {
			id = s;
			target = v;
			next = e;
		}

		Edge(String s) {
			this(s, null, null);
		}

		@Override
		public String toString() {
			return id;
		}

		// TODO!!! Your Edge methods here!

	} // Edge

	public class Graph {

		
		public void run() {
		Graph g = new Graph("G");
		g.createSimpleGraph(GraphTask.numberofedges, GraphTask.numberofedges);
		System.out.print("createRandomSimpleGraph:");
		System.out.println(g);

		// TODO!!! YOUR TESTS HERE!
	}
		
		
		String id;
		Vertex first;
		int info = 0;

		Graph(String s, Vertex v) {
			id = s;
			first = v;
		}

		Graph(String s) {
			this(s, null);
		}

		@Override
		public String toString() {
			String nl = System.getProperty("line.separator");
			StringBuffer sb = new StringBuffer(nl);
			sb.append(id);
			sb.append(nl);
			Vertex v = first;
			while (v != null) {
				sb.append(v.toString());
				sb.append(" -->");
				Edge e = v.first;
				while (e != null) {
					sb.append(" ");
					sb.append(e.toString());
					sb.append(" (");
					sb.append(v.toString());
					sb.append("->");
					sb.append(e.target.toString());
					sb.append(")");
					e = e.next;
				}
				sb.append(nl);
				v = v.next;
			}
			return sb.toString();
		}

		public Vertex createVertex(String vid) {
			Vertex res = new Vertex(vid);
			res.next = first;
			first = res;
			return res;
		}

		public Edge createArc(String eid, Vertex from, Vertex to) {
			Edge res = new Edge(eid);
			res.next = from.first;
			from.first = res;
			res.target = to;
			return res;
		}

		/**
		 * Create a connected undirected random tree with n vertices. Each new vertex is connected to some random existing vertex.
		 * 
		 * @param n
		 *            number of vertices added to this graph
		 */
		public void createTree(int n) {
			System.out.println("createRandomTree teeb puu niimitme vertexiga: " + n);
			if (n <= 0)
				return;
			Vertex[] varray = new Vertex[n]; // teeb Vertex tüüpi massiivi, suurusega n
			System.out.println(servad);
			
			for (int i = 0; i < varray.length; i++) {
				varray[i] = createVertex("v" + String.valueOf(i)); // oli (n - i)
			}
			
			int servadearv = servad.size() / 2;
			for (int i = 0; i < servadearv; i++) {
				int yks = servad.get(2 * i);
				int kaks = servad.get(2 * i + 1);
				
					createArc("e" + yks + "_" + kaks, varray[yks], varray[kaks]);
					createArc("e" + kaks + "_" + yks, varray[kaks], varray[yks]);
					
					System.out.println("i = " + 1 + " hetkel lisatav serv on " + yks + " + " + kaks);
//				if (i > 0) {
//					int vnr = (int) (Math.random() * i); // tähelepanu
//					System.out.println(GraphTask.edgesIn.get(i).toString().substring(4));
//				} else {
//				}
			}
		}

		/**
		 * Create an adjacency matrix of this graph. Side effect: corrupts info fields in the graph
		 * 
		 * @return adjacency matrix
		 */
		public int[][] createAdjMatrix() {
			info = 0;
			Vertex v = first;
			while (v != null) {
				v.info = info++;
				v = v.next;
			}
			int[][] res = new int[info][info];
			v = first;
			while (v != null) {
				int i = v.info;
				Edge e = v.first;
				while (e != null) {
					int j = e.target.info;
					res[i][j]++;
					e = e.next;
				}
				v = v.next;
			}
			return res;
		}

		/**
		 * Create a connected simple (undirected, no loops, no multiple edges) graph with n vertices and m edges.
		 * 
		 * @param n
		 *            number of vertices
		 * @param m
		 *            number of edges
		 */
		public void createSimpleGraph(int n, int m) {
			if (n <= 0)
				return;
			if (n > 2500)
				throw new IllegalArgumentException("Too many vertices: " + n);
			if (m < n - 1 || m > n * (n - 1) / 2)
				throw new IllegalArgumentException("Impossible number of edges: " + m);
			first = null;
			createTree(n); // n-1 edges created here
		}

		// TODO!!! Your Graph methods here!

	} // Graph

}

/**
 * Vertex class
 * 
 * @author vogella
 */
 class Verteks {
	final private String id;
	final private String name;

	/**
	 * Verxtex konstruktor
	 * 
	 * @param id String tyypi
	 * @param name String tyypi
	 * @author vogella
	 */
	public Verteks(String id, String name) {
		this.id = id;
		this.name = name;
	}

	/**
	 * Tagastab Vertexi id
	 * 
	 * @return String tyypi, id
	 * @author vogella
	 */
	public String getId() {
		return id;
	}

	/**
	 * Tagastab Vertexi nime
	 * 
	 * @return String tyypi, nimi
	 * @author vogella
	 */
	public String getName() {
		return name;
	}

	/**
	 * Equals meetod Vertexite v6rdlemiseks. Tagastab t6ev22rtuse.
	 * 
	 * @author vogella
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Verteks other = (Verteks) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	/**
	 * toString meetod, tagastab Vertexi nime
	 * 
	 * @author vogella
	 */
	@Override
	public String toString() {
		return name;
	}

}

/**
 * Edge class
 * 
 * author vogella
 */
class Edke {
	private final String id;
	private final Verteks source;
	private final Verteks destination;
	private final int weight;

	/**
	 * Serva konstruktor meetod
	 * 
	 * @param id String tyypi id
	 * @param source Vertex tyypi alguse tipp
	 * @param destination Vertex tyypi l6pu tipp
	 * @param weight int tyypi kaugus
	 * @author vogella
	 */
	public Edke(String id, Verteks source, Verteks destination, int weight) {
		this.id = id;
		this.source = source;
		this.destination = destination;
		this.weight = weight;
	}

	/**
	 * Tagastab id
	 * 
	 * @return
	 * @author vogella
	 */
	public String getId() {
		return id;
	}

	/**
	 * Tagastab destination'i
	 * 
	 * @return Vertex tyypi
	 * @author vogella
	 */
	public Verteks getDestination() {
		return destination;
	}

	/**
	 * Tagastab source'i
	 * 
	 * @return Vertex tyypi
	 * @author vogella
	 */
	public Verteks getSource() {
		return source;
	}

	/**
	 * Tagastab kauguse
	 * 
	 * @return int tyypi
	 */
	public int getWeight() {
		return weight;
	}

	@Override
	public String toString() {
		return source + " " + destination;
	}

}

/**
 * Graph class
 * 
 * @author vogella
 */
class Kraph {
	private final List<Verteks> vertexes;
	private final List<Edke> edges;

	/**
	 * Loob Graphi vertex'itest ja edge'idest
	 * 
	 * @param vertexes Vertex tyypi
	 * @param edges Vertex tyypi
	 * @author vogella
	 */
	public Kraph(List<Verteks> vertexes, List<Edke> edges) {
		this.vertexes = vertexes;
		this.edges = edges;
	}



	/**
	 * Tagastab vertex'id
	 * 
	 * @return vertexes Vertex tyypi
	 * @author vogella
	 */
	public List<Verteks> getVertexes() {
		return vertexes;
	}

	/**
	 * Tagastab edge'id
	 * 
	 * @return edges Vertex tyypi
	 * @author vogella
	 */
	public List<Edke> getEdges() {
		return edges;
	}

	

}

